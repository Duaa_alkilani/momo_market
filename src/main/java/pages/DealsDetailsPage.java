package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DealsDetailsPage extends PageBase {

    public DealsDetailsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "(//div[@class='product-card shadow-1 cursor-pointer'])[1]")
    WebElement item;
    @FindBy(xpath = "//p[@class='product-name mb-0 mobile-display-none mb-2']")
    WebElement descriptionOfItem;
    @FindBy(xpath = "(//span[@class='tag-now'])[1]")
    WebElement  price;
    @FindBy(xpath = "//button[@class='p-element mr-1 width-50 main-btn mobile-buy p-button p-component']")
    WebElement addToCardBtn;
    public void  clickItem(){
        item.click();
    }
    public  String  checkDescriptionOfItemNotEmpty(){
        return descriptionOfItem.getText();
    }
    public  String  checkPriceOfItemNotEmpty(){
        return price.getText();
    }
    public Boolean clickOnAddToCardBtn(){
       Boolean btnIsclickable= addToCardBtn.isEnabled();
       addToCardBtn.click();
        return btnIsclickable;
    }


}
