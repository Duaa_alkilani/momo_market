package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends PageBase{

    public CartPage(WebDriver driver) {
        super(driver);
    }
@FindBy(xpath = "//p[@class='product_name mb-0']")
    WebElement descrptionInCart;
    @FindBy(xpath = "//span[@class='tag-now']")
    WebElement priceInCart;

    public  String  checkDescriptionInCart(){
        return descrptionInCart.getText();
    }
    public  String  checkPriceOfItemInCart(){
        return priceInCart.getText();
    }
}
