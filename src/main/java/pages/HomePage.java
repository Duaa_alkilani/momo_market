package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends PageBase{
    public  HomePage(WebDriver driver){
        super(driver);
    }
    @FindBy(css = ".country-flag[src='https://momomarket.africa:1010/Images/countrytenant/ghana.png']")
    WebElement selectCountry;
    @FindBy(xpath = "//h2[normalize-space()='Hot Deals']")
    WebElement  hotDeals;
@FindBy(css = "button[class='p-element p-button-outlined view-all view-all-btn p-button p-component'] img[alt='No Image']")
WebElement viewAllHotDeals;
    public WebElement selectCountry(){
        return selectCountry;
    }


    public WebElement getHotDealstitleElement(){

   return hotDeals;
    }
    public WebElement clickOnViewAllDeals(){
      return viewAllHotDeals;
    }
    public static void scrollToSection(WebDriver driver, WebElement sectionElement) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView({ behavior: 'smooth', block: 'start' });", sectionElement);
    }
    @FindBy(xpath = "(//section[@class='section-block row'])[1]")
    // Scroll to the section
     WebElement sectionElement ;
    public WebElement sectionElement(){
        System.out.println(sectionElement.getText());
        return sectionElement;
    }


}
