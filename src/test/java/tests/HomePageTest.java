package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;

import java.time.Duration;


public class HomePageTest extends TestBase {
    HomePage homePage;

    @Test(priority = 1, description = "use the website as guest by select Ghana as country  ")
    public void useWebsiteAsGuest() {
        homePage = new HomePage(driver);
        homePage.selectCountry().click();
        String getTitle = driver.getTitle();
        Assert.assertEquals(getTitle, "Momo Market");
    }

    @Test(priority = 2, description = "go to scroll the between catgories to scroll to hot deals ")

    public void scrollToViewAllHotDeals() {

        homePage = new HomePage(driver);
        homePage.selectCountry().click();
        // Using JavaScript Executor
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.xpath("(//button[@type='button'])[2]"));
        js.executeScript("arguments[0].scrollIntoView(true);", element);
        //Using Actions Class
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click()
                .perform();

        // Scrolling Until an Element is Visible
        while (!element.isDisplayed()) {
            js.executeScript("window.scrollBy(0,900)");
        }

       WebElement listReturn=driver.findElement(By.xpath("//div[@class='font-size-22 bold-font']"));
        Wait<WebDriver> wait1 = new WebDriverWait(driver, Duration.ofSeconds(2));
        wait1.until(d -> listReturn.isDisplayed());
        String url = driver.getCurrentUrl();
        System.out.println(url);
        Assert.assertEquals(driver.getCurrentUrl(), "https://momomarket.africa/Portal/category/1&10000&Hot%20Deals");
    }

}


