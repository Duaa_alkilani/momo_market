package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CartPage;
import pages.DealsDetailsPage;
import pages.HomePage;
public class CartTest extends TestBase{
    DealsDetailsPage listPage;
    HomePage homePage;
    CartPage cartPage;

    @Test(description = "check the item has the same price && description in cart page  ")
    public void checkItmePlaceWithTheSamePriceAndDescription() throws InterruptedException {
        homePage = new HomePage(driver);

        listPage = new DealsDetailsPage(driver);
        cartPage =new CartPage(driver);

        homePage.selectCountry().click();
        goToHotDealsListing();
        listPage.clickItem();
        String priceInListing=listPage.checkPriceOfItemNotEmpty();
        String  descriptionInListing= listPage.checkDescriptionOfItemNotEmpty();
        listPage.clickOnAddToCardBtn();
        goToCartPage();
        Thread.sleep(100);
        String priceAfterAdd= cartPage.checkPriceOfItemInCart();
        String descriptionAfterAdd=cartPage.checkDescriptionInCart();
        System.out.println("  descriptionAfterAdd==    "+descriptionAfterAdd+ "   descriptionInListing==   "+descriptionInListing);
        Assert.assertEquals(descriptionAfterAdd,descriptionInListing) ;
        System.out.println("  priceAfterAdd==    "+priceAfterAdd+ "   priceInListing==   "+priceInListing);
        Assert.assertEquals(priceAfterAdd, priceInListing);

    }
}