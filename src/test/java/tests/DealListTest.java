package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.DealsDetailsPage;
import pages.HomePage;
public class DealListTest extends TestBase {
    DealsDetailsPage list;
    HomePage homePage;

    @Test(priority = 1,description = "click on item from list of hot deals  ")
    public void clickOnitemFromList() {
        homePage =new HomePage(driver);
         list= new DealsDetailsPage(driver);
        homePage.selectCountry().click();
        goToHotDealsListing();
        list.clickItem();
      String  descriptionOfItem=  list.checkDescriptionOfItemNotEmpty();
      String price= list.checkPriceOfItemNotEmpty();
      Assert.assertNotNull(descriptionOfItem);
        System.out.printf("descritpion == %s ",descriptionOfItem);
        System.out.printf("price==%S",price);
        Assert.assertNotNull(price);
    }
    @Test(priority = 2,description = "add a new to add item to card")
    public void addToCard(){
        homePage =new HomePage(driver);
        list= new DealsDetailsPage(driver);
        homePage.selectCountry().click();
        goToHotDealsListing();
        list.clickItem();
        Boolean btnStatus=list.clickOnAddToCardBtn();
        System.out.printf("btn is enable =%s",btnStatus);
        Assert.assertTrue(btnStatus);
    }

}