package tests;

import com.google.common.io.Files;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.DealsDetailsPage;
import pages.HomePage;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class TestBase {
  WebDriver driver;
  String urlForHomePage="https://momomarket.africa/Portal/";
    String urlForHotDeals="https://momomarket.africa/Portal/category/1&10000&Hot%20Deals";
    String urlForCartPage="https://momomarket.africa/Portal/cart";
    @BeforeMethod
    public void openBrowser(){
        driver = new ChromeDriver();

        driver.navigate().to(urlForHomePage);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
    }

    public void goToHotDealsListing(){
        driver.navigate().to(urlForHotDeals);
    }
    public void goToCartPage(){
        driver.navigate().to(urlForCartPage);
    }
    @AfterMethod
    public void recordFailure(ITestResult result) throws IOException {
        if (ITestResult.FAILURE == result.getStatus()) {
            TakesScreenshot camera = (TakesScreenshot) driver;
            File screenshot = camera.getScreenshotAs(OutputType.FILE);

            Files.move(screenshot, new File("/Users/duaaakram/Downloads/AutomationTask/momoMarketWeb/src/test/resoures/screenshots" + result.getName() + ".png"));
        }

        System.out.println("afterMethodTearDown");
        driver.quit();
    }

    }





